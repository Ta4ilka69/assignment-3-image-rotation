#ifndef UTIL

#include <stdio.h>

#define READ_MODE_BINARY "rb"
#define WRITE_MODE_BINARY "wb"
#define ANGLES_COUNT 7
#define printError(msg) fprintf(stderr,"%s",msg);

enum fileStatus {
    FILE_OPEN_OK = 0,
    FILE_OPEN_ERROR,
    FILE_CLOSE_OK,
    FILE_CLOSE_ERROR,
};

enum fileStatus openFile(FILE** file,const char* filename, const char* mode);
enum fileStatus closeFile(FILE* file);

static const char* fileStatusMessages[] = {
    "File has been opened",
    "Error on opening file",
    "File was closed successfully",
    "Error on closing file",
    "Unknown error"
};
int isValidAngle(int angle);
void onReturnError(FILE* file, const char* err, struct image* s, struct image* t);
#endif
