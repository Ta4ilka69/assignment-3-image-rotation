#ifndef TRANSFORM

#include <image.h>

void rotate(struct image* source, int angle, struct image* img);
void transpose(struct image* source, struct image* img);
void reverse(struct image* source, struct image* img);
struct image copyImage(struct image* source);
#endif
