#include <image.h>
#include <util.h>

const int angles[] = { 0, 90, 180, 270, -90, -180,-270 };
enum fileStatus openFile(FILE** file, const char* filename, const char* mode) {
	*file = fopen(filename, mode);
	if(*file == NULL) {
		return FILE_OPEN_ERROR;
	}
	return FILE_OPEN_OK;
}

enum fileStatus closeFile(FILE* file) {
	if (fclose(file) == 0) {
		return FILE_CLOSE_OK;
	}
	else {
		return FILE_CLOSE_ERROR;
	}
}
int isValidAngle(int angle) {
	for (int i = 0; i < ANGLES_COUNT; i++) {
		if (angles[i] == angle) {
			return 1;
		}
	}
	return 0;
}
void onReturnError(FILE* file, const char* err, struct image * s, struct image * t) {
	printError(err);
	closeFile(file);
	destroyImage(s);
	destroyImage(t);
}
