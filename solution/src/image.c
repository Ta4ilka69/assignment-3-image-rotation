#include <image.h>
#include <stdlib.h>
struct image createImage(uint64_t width, uint64_t height) {
	struct image img;
	img.width = width;
	img.height = height;
	img.data = malloc(sizeof(struct pixel)*width * height);
	return img;
}

void destroyImage(struct image *img) {
	if (img != NULL && img->data != NULL) {
		free(img->data);
		img->data = NULL;
	}
}
