#include <transform.h>

void rotate(struct image* source, int angle, struct image* img) {
    if (angle == 0) {
        *img = copyImage(source);
        return;
    }
    int times = angle / 90;

    if (times > 0) {
        for (int i = 0; i < times; i++) {
            struct image temp = { 0 };
            if (i == 0) {
                reverse(source, &temp);
            }
            else {
                reverse(img, &temp);\
                destroyImage(img);
            }
            transpose(&temp, img);
            destroyImage(&temp);
        }
    }
    else {
        for (int i = 0; i < -times; i++) {
            struct image temp = { 0 };
            if (i == 0) {
                transpose(source, &temp);
            }
            else {
                transpose(&temp, img);
                destroyImage(img);
            }
            reverse(&temp, img);
            destroyImage(&temp);
        }
    }
}

void transpose(struct image* source, struct image* img) {
    *img = createImage(source->height, source->width);
    for (int i = 0; i < source->height; i++) {
        for (int j = 0; j < source->width; j++) {
            img->data[j * img->width + i] = source->data[i * source->width + j];
        }
    }
}

void reverse(struct image* source, struct image* img) {
    *img = createImage(source->width, source->height);
    for (int i = 0; i < source->height; i++) {
        for (int j = 0; j < source->width; j++) {
            img->data[i * img->width + j] = source->data[i * source->width + source->width - j - 1];
        }
    }
}

struct image copyImage(struct image* source) {
    struct image img = createImage(source->width, source->height);
    for (int i = 0; i < source->height; i++) {
        for (int j = 0; j < source->width; j++) {
            img.data[i * img.width + j] = source->data[i * source->width + j];
        }
    }
    return img;
}
