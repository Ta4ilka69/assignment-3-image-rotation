#include <bmp.h>
#include <image.h>
#include <readWrite.h>
#include <stdlib.h>
#include <transform.h>
#include <util.h>

#define ARGS_COUNT 4
#define ERROR_CODE 1
#define SUCCESS_CODE 0



int main( int argc, char** argv ) {
    if (argc != ARGS_COUNT) {
        printError("USAGE: ./image-transformer <source-image> <transformed-image> <angle>\n");
        return ERROR_CODE;
    }
    char* sourcePath = argv[1];
    char* transformedPath = argv[2];
    int angle = atoi(argv[3]);
    if (!isValidAngle(angle)) {
        printError("Error: angle must be from this list: {0, 90, 180, 270, -90, -180,-270}");
        return ERROR_CODE;
    }
    FILE* sourceFile = NULL;
    enum fileStatus sourceOpen = openFile(&sourceFile, sourcePath, READ_MODE_BINARY);
    if (sourceOpen != FILE_OPEN_OK) {
        printError(fileStatusMessages[sourceOpen])
	}
    struct image sourceImage;
    enum readStatus sourceRead = fromBmp(sourceFile, &sourceImage);
    if(sourceRead != READ_OK) {
        onReturnError(sourceFile, readErrorMessages[sourceRead], &sourceImage, NULL);
        return ERROR_CODE;
	}
    enum fileStatus sourceClose = closeFile(sourceFile);
    if (sourceClose != FILE_CLOSE_OK) {
        onReturnError(sourceFile, fileStatusMessages[sourceClose], &sourceImage, NULL);
        return ERROR_CODE;
    }
    struct image transformedImage;
    rotate(&sourceImage, angle, &transformedImage);
    if(transformedImage.data == NULL) {
        onReturnError(NULL, "Error: memory allocation failed",&sourceImage,&transformedImage);
        return ERROR_CODE;
	}
    destroyImage(&sourceImage);
    FILE* transformedFile = NULL;
    enum fileStatus transformedOpen = openFile(&transformedFile, transformedPath, WRITE_MODE_BINARY);
    if (transformedOpen != FILE_OPEN_OK) {
        onReturnError(NULL, fileStatusMessages[transformedOpen], NULL, &transformedImage);
		return ERROR_CODE;
    }
    enum writeStatus transformedWrite = toBmp(transformedFile, &transformedImage);
    if (transformedWrite != WRITE_OK) {
		onReturnError(transformedFile, writeErrorMessages[transformedWrite], NULL, &transformedImage);
		return ERROR_CODE;
	}
    destroyImage(&transformedImage);
    enum fileStatus transformedClose = closeFile(transformedFile);
    if (transformedClose != FILE_CLOSE_OK) {
        onReturnError(NULL, fileStatusMessages[transformedClose],NULL,NULL);
        return ERROR_CODE;
    }
    return SUCCESS_CODE;
}
